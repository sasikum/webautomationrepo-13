package com.tatahealth.ReusableModules;

import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class reusableModules{
	
	public static int getcurrentWeekfromCalendar() throws Exception{
		
		Date date = new Date();
		String localDate = date.toString();
		System.out.println(localDate);
		int y=0;
		switch(localDate.substring(0, 3)) {
		case "Sun" :
			y=1;
			break;
		case "Mon":
			y=2;
			break;
		case "Tue":
			y=3;
			break;
		case "Wed":
			y=4;
			break;
		case "Thu":
			y=5;
			break;
		case "Fri":
			y=6;
			break;
		case "Sat":
			y=7;
			break;
		default:
			System.out.println("irrelevent");
		}
		return y;
	}
	
	
	
	public static int getcurrentDayfromCalendar(WebDriver driver) throws Exception{
		int n = 0;
		int week = getcurrentWeekfromCalendar();
		Date date = new Date();
		String localDate = date.toString().substring(8, 10);
		if(localDate.substring(0,1).equalsIgnoreCase("0")) {
			localDate = localDate.substring(1);
		}
		
		while(true) {
			n++;
			String cse = "tr:nth-child("+n+") > .day:nth-child("+week+")";
			if(driver.findElement(By.cssSelector(cse)).getText().equalsIgnoreCase(localDate)){
				break;
			}
			
		}
		
		return n;
	}
	
	
}
