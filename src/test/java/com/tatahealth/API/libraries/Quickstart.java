package com.tatahealth.API.libraries;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;

public class Quickstart {

	/** Application name. */
	private static final String APPLICATION_NAME = "Gmail API Java Quickstart";

	/** Directory to store user credentials for this application. */
	private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("./"),
			".credentials/gmail-java-quickstart");

	/** Global instance of the {@link FileDataStoreFactory}. */
	private static FileDataStoreFactory DATA_STORE_FACTORY;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	/** Global instance of the HTTP transport. */
	private static HttpTransport HTTP_TRANSPORT;
	private static final String SECRET_FILE_NAME = "./client_secret_reports.json";
	private static final List<String> MAIL_SCOPES = Collections.singletonList(GmailScopes.MAIL_GOOGLE_COM);
	/**
	 * Global instance of the scopes required by this quickstart.
	 *
	 * If modifying these scopes, delete your previously saved credentials at
	 * ~/.credentials/gmail-java-quickstart
	 */
	private static final List<String> SCOPES = Arrays.asList(GmailScopes.GMAIL_LABELS, GmailScopes.GMAIL_COMPOSE,
			GmailScopes.GMAIL_INSERT, GmailScopes.GMAIL_MODIFY, GmailScopes.GMAIL_READONLY, GmailScopes.GMAIL_SEND,
			GmailScopes.MAIL_GOOGLE_COM, GmailScopes.GMAIL_SETTINGS_SHARING, GmailScopes.GMAIL_SETTINGS_BASIC,
			DriveScopes.DRIVE_METADATA_READONLY, DriveScopes.DRIVE, DriveScopes.DRIVE_APPDATA, DriveScopes.DRIVE_FILE,
			SheetsScopes.SPREADSHEETS_READONLY, SheetsScopes.SPREADSHEETS);

	static {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
		} catch (Throwable t) {
			t.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @return an authorized Credential object.
	 * @throws Exception
	 */
	public static Credential authorize() throws Exception {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
	  	//InputStream in = classloader.getResourceAsStream("client_secret.json");
	  	InputStream in = new FileInputStream("./client_secret_reports.json");
		 
	  	Credential credential = GoogleCredential.fromStream(in).createScoped(SCOPES);
	      return credential;
		
	}

	/**
	 * Build and return an authorized Gmail client service.
	 * 
	 * @return an authorized Gmail client service
	 * @throws Exception
	 */
	public static Gmail getGmailService() throws Exception {
		Credential credential = authorize();
		return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	/**
	 * @author Bala Yaswanth Build and return an authorized Gmail client service.
	 * @return an authorized Drive client service
	 * @throws Exception
	 */
	public static Drive getDriveService() throws Exception {
		Credential credential = authorize();
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
	}

	/**
	 * Build and return an authorized Sheets API client service.
	 * 
	 * @return an authorized Sheets API client service
	 * @throws Exception
	 */
	public static Sheets getSheetsService() throws Exception {
		Credential credential = authorize();
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	public static void main(String[] args) throws Exception {

		List<Message> messages = GmailAPI.listMessagesMatchingQuery("me", "AUTOMATION-QA-CA-ANDROID");
		String messageid = messages.get(0).get("id").toString();
		GmailAPI.getAttachments("bala.gorrepati@tatahealth.com", messageid);

		/*
		 * String FolderId = DriveAPI.findFolderId(); String FileId =
		 * DriveAPI.getFileFromFolder(FolderId); DriveAPI.downloadFromDrive(FileId);
		 */

	}
	  public static Gmail getGmailServiceForServiceAccount(String serviceAccountUser)
				throws GeneralSecurityException, IOException {
			
			try {
				InputStream in = new FileInputStream(SECRET_FILE_NAME);
				final NetHttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
				GoogleCredential clientSecrets = GoogleCredential.fromStream(in, httpTransport, JSON_FACTORY);
				GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
						.setJsonFactory(JSON_FACTORY)
						.setServiceAccountPrivateKey(clientSecrets.getServiceAccountPrivateKey())
						.setServiceAccountPrivateKeyId(clientSecrets.getServiceAccountPrivateKeyId())
						.setServiceAccountScopes(MAIL_SCOPES).setServiceAccountId(clientSecrets.getServiceAccountId())
						.setServiceAccountUser(serviceAccountUser)
						.setServiceAccountProjectId(clientSecrets.getServiceAccountProjectId()).build();
				return new Gmail.Builder(httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
						.build();
			}finally{
				//in.close();
			}
		}

}
