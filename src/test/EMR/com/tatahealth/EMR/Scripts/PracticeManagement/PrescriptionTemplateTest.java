
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.PrescriptionTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.EMR.pages.PracticeManagement.PrescriptionTemplatePages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class PrescriptionTemplateTest {
	
	public static String templateName = "";
	public static String frequencyText ="";
	public static String durationText ="";
	public static String doseText ="";
	public static List<String> allSavedMedicinesInTemplate =new ArrayList<>();
	public static String savedMedicineTableDetails ="";
	ExtentTest logger;
	
	
	@BeforeClass(alwaysRun=true,groups= {"Regression"})
		public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement_Prescription";
			Login_Doctor.LoginTest();
			new MedicalKItTest().closePreviousConsultations();
			new PracticeManagementTest().moveToPracticeManagement();
		} 
		
		@AfterClass(alwaysRun=true,groups = { "Regression"})
		public static void afterClass() {
				Login_Doctor.LogoutTest();
			}
		
		//PM_4 ,PM_5 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=403)
		public synchronized void verifyPrescriptionSection()throws Exception{
			logger = Reports.extent.createTest("EMR validate Precription Section");
			PrescriptionPage prescription = new PrescriptionPage();
			PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
			PracticeManagementPages pmp = new PracticeManagementPages();
			Web_GeneralFunctions.click(pmp.getLeftMainMenu(Login_Doctor.driver, logger), "click on left menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getPrescriptionMenu(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getPrescriptionSectionHeader(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getCreateTemplate(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getTemplateSearchField(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isChecked(prescription.getCIMSCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver));
			assertTrue(Web_GeneralFunctions.isChecked(prescription.getTDHCheckBox(Login_Doctor.driver, logger), Login_Doctor.driver));
			assertTrue(Web_GeneralFunctions.isChecked(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger), Login_Doctor.driver));
			assertTrue(Web_GeneralFunctions.getText(ptp.getCIMSLabel(Login_Doctor.driver, logger),"get Cims label text",Login_Doctor.driver,logger).trim().equals("CIMS"));
			assertTrue(Web_GeneralFunctions.getText(ptp.getTDHCatalogLabel(Login_Doctor.driver, logger),"get TDH catalog label text",Login_Doctor.driver,logger).trim().equals("TDH Catalog"));
			assertTrue(Web_GeneralFunctions.getText(ptp.getClinicMasterLabel(Login_Doctor.driver, logger),"get clinic master label text",Login_Doctor.driver,logger).trim().equals("Clinic Master"));

		}
		
		//PM_6 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=404)
		public synchronized void checkAllMedicineMasterTypes() throws Exception {
			logger = Reports.extent.createTest("EMR Select all medicine master types and validate");
			PrescriptionPage prescription = new PrescriptionPage();
			if(!Web_GeneralFunctions.isChecked(prescription.getCIMSCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver)){
				Web_GeneralFunctions.click(prescription.getCIMSCheckBox(Login_Doctor.driver, logger), "check CIMS checkbox", Login_Doctor.driver, logger);
				assertTrue(Web_GeneralFunctions.isChecked(prescription.getCIMSCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver));
			}
			if(!Web_GeneralFunctions.isChecked(prescription.getTDHCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver)){
				Web_GeneralFunctions.click(prescription.getTDHCheckBox(Login_Doctor.driver, logger), "check TDH checkbox", Login_Doctor.driver, logger);
				assertTrue(Web_GeneralFunctions.isChecked(prescription.getTDHCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver));
			}
			if(!Web_GeneralFunctions.isChecked(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver)){
				Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "check clinic specific checkbox", Login_Doctor.driver, logger);
				assertTrue(Web_GeneralFunctions.isChecked(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger),Login_Doctor.driver));
			}
		}
		
		//PM_8 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=405)
		public synchronized void validateSelectedMedicineMasterDisplayInPrescriptionSection() throws Exception {
			logger = Reports.extent.createTest("EMR Check only Selected Medicine master is displayed in prescription section");
			PracticeManagementPages pmp = new PracticeManagementPages();
			PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
			Web_GeneralFunctions.click(pmp.getLeftMainMenu(Login_Doctor.driver, logger), "click on left menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ptp.getMasterAdministrationLinkInLeftNav(Login_Doctor.driver, logger) );
			Web_GeneralFunctions.click(ptp.getMasterAdministrationLinkInLeftNav(Login_Doctor.driver, logger), "click on master admin link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger),"click on configuration master",Login_Doctor.driver,logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger), "click on medicine master edit link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger));
			if(Web_GeneralFunctions.isChecked(ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				Web_GeneralFunctions.click(ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), "unselect clinic specific checkbox", Login_Doctor.driver, logger);
			}
			Web_GeneralFunctions.click(ptp.getSaveButtonInConfigurationMaster(Login_Doctor.driver, logger), "click on save button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(pmp.getPracticeManagementMenu(Login_Doctor.driver, logger), "click on practicemanagemnt menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTDHCatalogLabel(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getTDHCatalogLabel(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getCIMSLabel(Login_Doctor.driver, logger)));
			assertFalse(Web_GeneralFunctions.isDisplayed(ptp.getClinicMasterLabel(Login_Doctor.driver, logger)));
			Web_GeneralFunctions.click(ptp.getMasterAdministrationLinkInLeftNav(Login_Doctor.driver, logger), "click on master admin link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger),"click on configuration master",Login_Doctor.driver,logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger), "click on medicine master edit link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger));
			if(!Web_GeneralFunctions.isChecked(ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				Web_GeneralFunctions.click(ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), "select clinic specific checkbox", Login_Doctor.driver, logger);
			}
			if(Web_GeneralFunctions.isChecked(ptp.getCIMSMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				Web_GeneralFunctions.click(ptp.getCIMSMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), "unselect CIMS checkbox", Login_Doctor.driver, logger);
			}
			Web_GeneralFunctions.click(ptp.getSaveButtonInConfigurationMaster(Login_Doctor.driver, logger), "click on save button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(pmp.getPracticeManagementMenu(Login_Doctor.driver, logger), "click on practicemanagemnt menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTDHCatalogLabel(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getTDHCatalogLabel(Login_Doctor.driver, logger)));
			assertFalse(Web_GeneralFunctions.isDisplayed(ptp.getCIMSLabel(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getClinicMasterLabel(Login_Doctor.driver, logger)));
			Web_GeneralFunctions.click(ptp.getMasterAdministrationLinkInLeftNav(Login_Doctor.driver, logger), "click on master admin link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getConfigurationMasterInMasterAdministration(Login_Doctor.driver, logger),"click on configuration master",Login_Doctor.driver,logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ptp.getMedicineMasterEditlink(Login_Doctor.driver, logger), "click on medicine master edit link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getClinicSpecificMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger));
			if(!Web_GeneralFunctions.isChecked(ptp.getCIMSMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				Web_GeneralFunctions.click(ptp.getCIMSMasterCheckboxInConfigurationMaster(Login_Doctor.driver, logger), "select CIMS checkbox", Login_Doctor.driver, logger);
			}
			Web_GeneralFunctions.click(ptp.getSaveButtonInConfigurationMaster(Login_Doctor.driver, logger), "click on save button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(pmp.getPracticeManagementMenu(Login_Doctor.driver, logger), "click on practicemanagemnt menu", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTDHCatalogLabel(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getTDHCatalogLabel(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getCIMSLabel(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getClinicMasterLabel(Login_Doctor.driver, logger)));
		}
		
		//PM_20 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=406)	
		public synchronized void verifyTemplateNameFieldLimitation() throws Exception {
				logger = Reports.extent.createTest("EMR verify template Name field limitation ");
				PrescriptionPage prescription = new PrescriptionPage();
				PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
				PrescriptionTest pTest = new PrescriptionTest();
				PracticeManagementPages pmp = new PracticeManagementPages();
				Web_GeneralFunctions.click(pmp.getLeftMainMenu(Login_Doctor.driver, logger), "click on left menu", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ptp.getCreateTemplate(Login_Doctor.driver, logger), "Click Create Template button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTemplateNameField(Login_Doctor.driver, logger));
				templateName = "!@#$%^&*()-_=+}{[]'?."+RandomStringUtils.randomAlphanumeric(150);
				Web_GeneralFunctions.sendkeys(ptp.getTemplateNameField(Login_Doctor.driver, logger), templateName, "Enter template name", Login_Doctor.driver, logger);
				PrescriptionTest.row=1;
				pTest.selectMedicine();
				pTest.enterFrequency();
				durationText = RandomStringUtils.randomAlphanumeric(10);
				Web_GeneralFunctions.sendkeys(prescription.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), durationText, "sending text to duration field", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ptp.getSaveTemplateButton(Login_Doctor.driver, logger), "click on savetemplate button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
				Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
				String extractedTemplateName = Web_GeneralFunctions.getText(prescription.getTemplateName(templateName.substring(0,3), Login_Doctor.driver, logger),"extracting template Name ", Login_Doctor.driver, logger);
				assertTrue(templateName.substring(0,150).equals(extractedTemplateName));
				Web_GeneralFunctions.click(ptp.getPrescriptionMenu(Login_Doctor.driver, logger), "click on prescription menu", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTemplateSearchField(Login_Doctor.driver, logger));
				Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(prescription.getTemplateName(templateName.substring(0,3), Login_Doctor.driver, logger), "select saved template", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ptp.getDeleteTemplateButton(Login_Doctor.driver, logger), "deleting created template", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getYesInDeleteTemplatePopUp(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ptp.getYesInDeleteTemplatePopUp(Login_Doctor.driver, logger), "click on yes in delete pop up", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getCreateTemplate(Login_Doctor.driver, logger));
				assertTrue(extractedTemplateName.length()==150);
		}
		
		//PM_15 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=407)	
	public synchronized void verifyCreateTemplate() throws Exception {
			logger = Reports.extent.createTest("EMR verify create template ");
			PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
			Web_GeneralFunctions.click(ptp.getCreateTemplate(Login_Doctor.driver, logger), "Click Create Template button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTemplateNameField(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getTemplateNameField(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getSaveTemplateButton(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getAddMedicineButton(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getMedicineTable(Login_Doctor.driver, logger)));
			
		}
		
		//PM_17 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=408)
	public synchronized void saveTemplateWithEmptyTemplateName()throws Exception{
		logger = Reports.extent.createTest("EMR Save template with empty template Name");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		saveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getSaveTemplateWithEmptyDataMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getSaveTemplateWithEmptyDataMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter template name"));
		Web_GeneralFunctions.wait(3);
	}
	
	//PM_18 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=409)
	public synchronized void saveTemplateWithEmptyMedicineData()throws Exception{
		
		logger = Reports.extent.createTest("EMR Save template with empty medicine data");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		templateName = RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(ptp.getTemplateNameField(Login_Doctor.driver, logger), templateName, "Enter template name", Login_Doctor.driver, logger);
		saveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getSaveTemplateWithEmptyMedicineDataMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getSaveTemplateWithEmptyMedicineDataMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter at least one medicine"));
		Web_GeneralFunctions.wait(3);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=410)
	public synchronized void deleteMedicineRowWithEmptyData()throws Exception{
		logger = Reports.extent.createTest("EMR delete medicine row with empty data");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(ptp.getDeleteRow(Login_Doctor.driver, logger), "Click medicine row delete button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteRowWithEmptyDataMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getDeleteRowWithEmptyDataMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter prescription"));
		Web_GeneralFunctions.wait(3);
	}
	
	//PM_46 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=411)
	public synchronized void addMedicineRowWithExistingEmptyRow()throws Exception{
		logger = Reports.extent.createTest("EMR add medicine with empty data");
		PrescriptionTest pTest = new PrescriptionTest();
		pTest.emptyRowAlreadyExist();
		Web_GeneralFunctions.wait(3);
	}
	
	//PM_10 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=412)
	public synchronized void validateOrderOfMedicineCategoryInSuggestions() throws Exception {
		logger = Reports.extent.createTest("EMR validate medicine category order");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionTest.row = 1;
		List<String> expectedMedicinesCategoriesOrder = Arrays.asList(new String[] {"Clinic Master","TDH Catalog","CIMS"});
		PrescriptionPage pp = new PrescriptionPage();
		Web_GeneralFunctions.click(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "click on drug text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineCategorySuggestionWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> allMedicineCategoriesList = ptp.getAllMedicineCategorySuggestionNamesAsList(Login_Doctor.driver, logger);
		List<String> actualMedicinesCategoriesOrder  =allMedicineCategoriesList.stream().distinct().collect(Collectors.toList());
		assertTrue(actualMedicinesCategoriesOrder.equals(expectedMedicinesCategoriesOrder));
	}
	
	//PM_21 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=413)
		public synchronized void verifyDisplayOfRelatedMedicines() throws Exception {
		logger = Reports.extent.createTest("EMR verify display of related medicines");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "Tablet", "search with tablet text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineCategorySuggestionWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> tabletsSuggestionList= ptp.getAllMedicinesNamesAsList(Login_Doctor.driver, logger);
		for(String medicine :tabletsSuggestionList) {
			assertTrue(medicine.toLowerCase().contains("tablet"));
		}
		Web_GeneralFunctions.click(ptp.getMedicineSearchField(Login_Doctor.driver, logger),"click on medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clearing medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "Syrup", "search with syrup text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineCategorySuggestionWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> syrupSuggestionsList = ptp.getAllMedicinesNamesAsList(Login_Doctor.driver, logger);
		for(String medicine :syrupSuggestionsList) {
			assertTrue(medicine.toLowerCase().contains("syrup"));
		}
		Web_GeneralFunctions.click(ptp.getMedicineSearchField(Login_Doctor.driver, logger),"click on medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clearing medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "Injection", "search with injection text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineCategorySuggestionWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> injectionSuggestionList= ptp.getAllMedicinesNamesAsList(Login_Doctor.driver, logger);
		for(String medicine :injectionSuggestionList) {
			assertTrue(medicine.toLowerCase().contains("injection"));
		}
	
	}
	
	//PM_19 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=414)
	public synchronized void saveTemplateWithEmptyFrequency()throws Exception{
		logger = Reports.extent.createTest("EMR save template with empty frequency");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		pTest.selectMedicine();
		pTest.enterFrequency();
		Web_GeneralFunctions.clear(pPage.getFrequency(PrescriptionTest.row,Login_Doctor.driver , logger), "clear frequency", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getFrequencyLabel(Login_Doctor.driver, logger),"clicking on Frequency label",Login_Doctor.driver,logger);
		pTest.enterDuration();
		saveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
	}
	
	//PM_38 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=415)
	public synchronized void verifyDisplayOfDefaultFrequencyList() throws Exception {
		logger = Reports.extent.createTest("EMR verify Display of default frequency list");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionPage pp = new PrescriptionPage();
		Web_GeneralFunctions.clearWebElement(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "","Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllFrequencySuggestionsAsWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> actualFrequencySuggestions = ptp.getAllFrequenciesAsList(Login_Doctor.driver, logger);
		List<String> defaultFrequencySuggestions = ptp.getDefaultFrequencyList();
		assertTrue(actualFrequencySuggestions.equals(defaultFrequencySuggestions));
		
}
	
	//PM_39 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=416)
	public synchronized void verifyDisplayOfSearchRelatedFrequencyInDropDown() throws Exception {
		logger = Reports.extent.createTest("EMR verify display of search related frequency");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionPage pp = new PrescriptionPage();
		Web_GeneralFunctions.clearWebElement(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		String frequencySearchString1 ="1-1";
		Web_GeneralFunctions.sendkeys(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), frequencySearchString1,"Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllFrequencySuggestionsAsWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> frequencySuggestionList1 = ptp.getAllFrequenciesAsList(Login_Doctor.driver, logger);
		for(String frequency: frequencySuggestionList1) {
			assertTrue(frequency.contains(frequencySearchString1));
		}
		Web_GeneralFunctions.clearWebElement(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		String frequencySearchString2 ="0-0";
		Web_GeneralFunctions.sendkeys(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), frequencySearchString2,"Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllFrequencySuggestionsAsWebElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> frequencySuggestionList2 = ptp.getAllFrequenciesAsList(Login_Doctor.driver, logger);
		for(String frequency: frequencySuggestionList2) {
			assertTrue(frequency.contains(frequencySearchString2));
		}
	}
	//PM_40 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=417)
	public synchronized void enterRandomFrequencyAndVerify()throws Exception{
		logger = Reports.extent.createTest("EMR enter Random Value for frequency");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		frequencyText =RandomStringUtils.randomAlphanumeric(5);
		Web_GeneralFunctions.sendkeys(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), frequencyText,"Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getFrequencyLabel(Login_Doctor.driver, logger),"clicking on Frequency label",Login_Doctor.driver,logger);
	}
	
	//PM_19 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=418)
	public synchronized void saveTemplateWithEmptyDuration()throws Exception{
		logger = Reports.extent.createTest("EMR save template with empty duration");
		PrescriptionPage pPage = new PrescriptionPage();
		Web_GeneralFunctions.click(pPage.getDuration(PrescriptionTest.row,Login_Doctor.driver, logger), "focus duration element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(pPage.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "Clear duration element", Login_Doctor.driver, logger);
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		saveTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
	}
	
	//PM_41 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=419)
	public synchronized void verifyDurationField() throws Exception {
		logger = Reports.extent.createTest("EMR validate duration field");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "clear frequency field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "","Sending frequnecy in medicine module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getRegularlyDurationElement(Login_Doctor.driver, logger));
		String durationName = Web_GeneralFunctions.getText(ptp.getRegularlyDurationElement(Login_Doctor.driver, logger), "duration field text", Login_Doctor.driver, logger);
		assertTrue(durationName.equals("Regularly"));
		assertTrue(ptp.getDaysDurationUnit(PrescriptionTest.row, Login_Doctor.driver, logger).isSelected());
	}
	
	//PM_42 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=420)
	public synchronized void verifyDurationLimitation() throws Exception {
		logger = Reports.extent.createTest("EMR validate duration field limitation");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "click on duration field", Login_Doctor.driver, logger);
		durationText = RandomStringUtils.randomAlphanumeric(21);
		Web_GeneralFunctions.sendkeys(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), durationText, "sending text to duration field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getSaveTemplateButton(Login_Doctor.driver, logger), "click on savetemplate button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Prescription Template Saved Successfully"));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		PrescriptionTest.row=0;
		String extractedFrequencyText = Web_GeneralFunctions.getAttribute(pp.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "extract frequency text", Login_Doctor.driver, logger);
		String extractedDurationText = Web_GeneralFunctions.getAttribute(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "extract duration text", Login_Doctor.driver, logger);
		System.out.println("**************************");
		System.out.println(extractedFrequencyText);
		System.out.println(frequencyText);
		assertTrue(extractedFrequencyText.equalsIgnoreCase(frequencyText));
		if(extractedDurationText.equals(durationText) || extractedDurationText.length()>20) {
			assertTrue(false);
		}
		else {
			assertTrue(true);
		}
	}
	
	//PM_22 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=421)
	public synchronized void saveMedicineWithCustomMedicineName() throws Exception {
		logger = Reports.extent.createTest("EMR Save medicine with custom medicine name");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clear medicine field", Login_Doctor.driver, logger);
		String randomMedicineText = RandomStringUtils.randomAlphanumeric(15);
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), randomMedicineText, "send random medicne text", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String extractedMedicineText = Web_GeneralFunctions.getAttribute(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "value", "extract medicine text", Login_Doctor.driver, logger);
		assertTrue(extractedMedicineText.equalsIgnoreCase(randomMedicineText));
	}
	
	//PM_16 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=422)
	public synchronized void saveTemplateWithExistingTemplateName() throws Exception {
		logger = Reports.extent.createTest("EMR save template with existing template Name");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(ptp.getCreateTemplate(Login_Doctor.driver, logger), "click on create template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getSaveTemplateButton(Login_Doctor.driver, logger));
		PrescriptionTest.row++;
		pTest.selectMedicine();
		pTest.enterFrequency();
		pTest.enterDuration();
		Web_GeneralFunctions.sendkeys(ptp.getTemplateNameField(Login_Doctor.driver, logger), templateName, "enter templateName text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getSaveTemplateButton(Login_Doctor.driver, logger), "click on save template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		PrescriptionTest.row--;
		assertTrue(text.equalsIgnoreCase("This Prescription Template name already exists. Please try another name"));
	}
	
	//PM_36 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=423)
	public synchronized void verifyDoseUnitsInDropDown() throws Exception {
		logger = Reports.extent.createTest("EMR validate Dose Units in DropDown");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger), "click on dose unit field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMLDoseUnit(PrescriptionTest.row, Login_Doctor.driver, logger));
		List<String> expectedDoseUnitsList = ptp.getExpectedDoseUnitsAsList();
		List<String> actualDoseUnitsList = ptp.getAllDoseUnitsAsList(PrescriptionTest.row, Login_Doctor.driver, logger);
		assertTrue(expectedDoseUnitsList.equals(actualDoseUnitsList));
	}
	
	
	//PM_25, PM_29 , PM_35 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=424)
	public synchronized void saveTemplateWithEmptyDoseField() throws Exception {
		logger = Reports.extent.createTest("EMR Save template with empty dose field");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		assertTrue(Web_GeneralFunctions.isDisplayed(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger)));
		Web_GeneralFunctions.clearWebElement(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dose field", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Prescription Template Updated Successfully"));
			
	}
	
	//PM_26 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=425)
	public synchronized void validateDoseFieldEmptinessforSavedTemplate() throws Exception {
		logger = Reports.extent.createTest("EMR validate dose field emptyness for saved template");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String doseFieldText = Web_GeneralFunctions.getAttribute(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger),"value", "extracting dose field text", Login_Doctor.driver, logger);
		assertTrue(doseFieldText.isEmpty());
	}
	
	//PM_37 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=426)
	public synchronized void verifyDoseFieldLimitation() throws Exception {
		logger = Reports.extent.createTest("EMR validate dose field limitation");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		doseText =  RandomStringUtils.randomAlphabetic(5);
		Web_GeneralFunctions.clearWebElement(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dosefield", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), doseText, "Enter dose", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String doseFieldText = Web_GeneralFunctions.getAttribute(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger),"value", "extracting dose field text", Login_Doctor.driver, logger);
		assertTrue(!doseFieldText.equals(doseText));
		doseText = "45.5676";
		Web_GeneralFunctions.clearWebElement(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dosefield", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), doseText, "Enter dose", Login_Doctor.driver, logger);
		pTest.selectDoseUnit();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String doseFieldText2 = Web_GeneralFunctions.getAttribute(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger),"value", "extracting dose field text", Login_Doctor.driver, logger);
		assertTrue(!doseFieldText2.equals(doseText));
		assertTrue(doseFieldText2.equals("45.56"));
	}
	
	//PM_27, PM_28,PM_24  covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=427)
	public synchronized void updateDoseFieldAndVerify() throws Exception {
		logger = Reports.extent.createTest("EMR Update dose field and validate");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "click on dose field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dose field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dosefield", Login_Doctor.driver, logger);
		Double d = RandomUtils.nextDouble();
		doseText = d.toString();
		Web_GeneralFunctions.sendkeys(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), doseText, "Enter dose", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger), "click on dose unit", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		pTest.selectDoseUnit();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String doseValue = Web_GeneralFunctions.getAttribute(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger),"value", "extracting dose field text", Login_Doctor.driver, logger);
		assertTrue(doseValue.equals(doseText.substring(0, 4)));
	}
	
	//PM_31 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=428)
	public synchronized void verifyDoseFieldPosition() {
		logger = Reports.extent.createTest("EMR validate Dose Field position");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		List<WebElement> medicineTableColumnsList = ptp.getColumnsListInMedicineTable(Login_Doctor.driver, logger);
		assertTrue(medicineTableColumnsList.get(0).getText().equals("Medicine") && medicineTableColumnsList.get(1).getText().equals("Dose"));
	}
	
	//PM_30 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=429)
	public synchronized void verifyMLIsDefaultUnitForSyrup() throws Exception {
		logger = Reports.extent.createTest("EMR validate Default dose unit for syrup");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clearing medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "Syrup", "search with syrup text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.select1stMedicineFromDropDown(PrescriptionTest.row.toString(), Login_Doctor.driver, logger), "select first syrup medicine", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		/*failing due to manual issue*/
		assertTrue(ptp.getMLDoseUnit(PrescriptionTest.row, Login_Doctor.driver, logger).isSelected());
	}
	
	//PM_34 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=431)
	public synchronized void selectUnitOtherThanDefaultUnitForDose() throws Exception {
		logger = Reports.extent.createTest("EMR Select different dose unit");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.click(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger), "click on dose unit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElement(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger), "Units", "select Litre dose unit", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String value = Web_GeneralFunctions.getAttribute(ptp.getLitresDoseUnit(PrescriptionTest.row, Login_Doctor.driver, logger), "selected", "extract property value", Login_Doctor.driver, logger);
		assertTrue(value.equalsIgnoreCase("true"));
	}
	
	//PM_33 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=430)
	public synchronized void SelectTabletMedicineAndVerifyDoseDefaultUnit() throws Exception {
		logger = Reports.extent.createTest("EMR validate Dose default unit for tablet");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clear(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clear medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "clearing medicine search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getMedicineSearchField(Login_Doctor.driver, logger), "tablet", "search with syrup text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.select1stMedicineFromDropDown(PrescriptionTest.row.toString(), Login_Doctor.driver, logger), "select first syrup medicine", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "click on dose field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dose field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "clear dosefield", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		assertTrue(ptp.getSelectDoseUnit(Login_Doctor.driver, logger).isSelected());

	}
	
	
	//PM_43 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=432)
	public synchronized void validateAdviceField()throws Exception{
		logger = Reports.extent.createTest("EMR Validate Advice field");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		Web_GeneralFunctions.clearWebElement(pp.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "clear duration field", Login_Doctor.driver, logger);
		pTest.enterDuration();
		Web_GeneralFunctions.click(ptp.getAdviceUnitDropDownElement(PrescriptionTest.row, Login_Doctor.driver, logger), "click on advice dropdown", Login_Doctor.driver, logger);
		List<String> expectedAdviceUnitasList = ptp.getExpectedAdviceUnitsAsList();
		List<String> actualAdviceUnitsasList = ptp.getAllAdviceUnitsAsList(PrescriptionTest.row, Login_Doctor.driver, logger);
		assertTrue(ptp.getAfterFoodAdviceUnitOption(PrescriptionTest.row, Login_Doctor.driver, logger).isSelected());
		Web_GeneralFunctions.selectElement(ptp.getAdviceUnitDropDownElement(PrescriptionTest.row, Login_Doctor.driver, logger), "Empty Stomach", "select empty stomach from advice dropdown", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		assertTrue(ptp.getEmptyStomachAdviceUnitOption(PrescriptionTest.row, Login_Doctor.driver, logger).isSelected());
		assertTrue(expectedAdviceUnitasList.equals(actualAdviceUnitsasList));
	}
	
	//PM_44 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=433)
	public synchronized void validateInstructionField()throws Exception{
		logger = Reports.extent.createTest("EMR Validate instruction field");
		PrescriptionPage pp = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getInstructionFieldTextBox(PrescriptionTest.row, Login_Doctor.driver, logger)));
		String randomInstructionText =RandomStringUtils.randomAlphanumeric(290)+"@#$%^&*(){}-_";
		Web_GeneralFunctions.click(ptp.getInstructionFieldTextBox(PrescriptionTest.row, Login_Doctor.driver, logger), "click on instruction text box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getInstructionFieldTextBox(PrescriptionTest.row, Login_Doctor.driver, logger),randomInstructionText , "enter random alphanumeric text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
		JavascriptExecutor js = (JavascriptExecutor)Login_Doctor.driver;
		String actualAdviceText = js.executeScript("return document.getElementById(\"freqSpecInstruction0\").value;").toString();
		assertTrue(actualAdviceText.trim().length()==300);
		assertTrue(randomInstructionText.substring(0,300).equals(actualAdviceText.trim()));
	
	}
	
	//PM_47 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=434)
		public synchronized void deleteMedicineAndValidate()throws Exception{
			logger = Reports.extent.createTest("EMR Delete medicine and verify");
			PrescriptionPage pPage = new  PrescriptionPage();
			PrescriptionTest pTest = new PrescriptionTest();
			PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
			pTest.addMedicine();
			PrescriptionTest.row++;
			assertTrue(ptp.getMedicineTableRowsList(Login_Doctor.driver, logger).size()==2);
			pTest.selectMedicine();
			Web_GeneralFunctions.click(pPage.getDeleteRow(PrescriptionTest.row, Login_Doctor.driver, logger), "click on delete row", Login_Doctor.driver, logger);
			assertTrue(ptp.getMedicineTableRowsList(Login_Doctor.driver, logger).size()==1);
			updateTemplate();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
			PrescriptionTest.row--;
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
			assertTrue(ptp.getMedicineTableRowsList(Login_Doctor.driver, logger).size()==1);
		}
		
	//PM_45 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=435)
	public synchronized void saveTemplateWithEmptyMedicineName()throws Exception{
		logger = Reports.extent.createTest("EMR save template with empty medicine name");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		pTest.addMedicine();
		assertTrue(ptp.getMedicineTableRowsList(Login_Doctor.driver, logger).size()==2);
		PrescriptionTest.row++;
		pTest.selectFrequnecyFromMasterList();
		pTest.selectRegularlyDurationFromMasterList();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
		
	}
		//PM_9 ,PM_11 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=436)
	public synchronized void SelectOnlyTDHMedicineAndVerifyMedicineCatalog()throws Exception{
		logger = Reports.extent.createTest("EMR Select only TDH medicine And validate medicine catalog");
		chooseOnlyTDHMedicine();
	}
	
	
	@Test(groups= {"Regression","PracticeManagement"},priority=437)
	public synchronized void verifyDoseFieldDisplay()throws Exception{
		logger = Reports.extent.createTest("EMR validte dose field display");
		PrescriptionPage pp = new PrescriptionPage();
		assertTrue(Web_GeneralFunctions.isDisplayed(pp.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(pp.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger)));
	
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=438)
	public synchronized void saveTemplateWithEmptyDoseUnit()throws Exception{
		logger = Reports.extent.createTest("EMR save template with empty dose unit");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		pTest.enterDose();
		pTest.enterSplInstruction();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(ptp.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
		pTest.selectDoseUnit();
	}
	
	//PM_9 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=439)
	public synchronized void SelectOnlyCIMSMedicineAndVerifyMedicineCatalog()throws Exception{
		logger = Reports.extent.createTest("EMR Select only cims medicine and validate");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionTest.cimsMedicineName = "GENTARIL INJ 80mg";
		chooseOnlyCIMSMedicine();
		pTest.enterFrequency();
		pTest.durationInWeeks();
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=440)
	public synchronized void drugToDrugInteractionPopUp()throws Exception{
		logger = Reports.extent.createTest("EMR validate drug to drug interaction modal");
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionTest.interactionMedicine = "CROCID ORAL SUSP";
		pTest.drugToDrugInteraction();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getNOInInteractionPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getYesInInteractionPopUp(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=441)
	public synchronized void clickNoInDrugToDrugInteractionPopup()throws Exception{
		logger = Reports.extent.createTest("EMR click No in Drug to drug interaction modal and validate");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionPage pPage = new PrescriptionPage();
		Web_GeneralFunctions.click(ptp.getNOInInteractionPopUp(Login_Doctor.driver, logger), "click No button in interaction pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger));
		String attr = Web_GeneralFunctions.getAttribute(pPage.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "Get drug name", Login_Doctor.driver, logger);
		assertTrue(attr.isEmpty());
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=442)
	public synchronized void clickYesInDrugToDrugInteractionPopup()throws Exception{
		logger = Reports.extent.createTest("EMR click yes in drug to drug interaction modal");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTest.interactionMedicine = "CROCID ORAL SUSP";
		Web_GeneralFunctions.click(pPage.getFirstMedicineFromDropDown(PrescriptionTest.row, PrescriptionTest.interactionMedicine, Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getNOInInteractionPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getYesInInteractionPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(ptp.getYesInInteractionPopUp(Login_Doctor.driver, logger), "click Yes button in interaction pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		pTest.enterFrequency();
		pTest.enterDuration();
		pTest.enterSplInstruction();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(pPage.getMessage(Login_Doctor.driver, logger), "Get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Prescription Template Updated Successfully"));
		
	}
	
	//PM_51 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=443)
	public synchronized void verifyOrderOfPrescription()throws Exception{
		logger = Reports.extent.createTest("EMR validate order of saved prescription");
		PrescriptionPage pPage = new PrescriptionPage();
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
	Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
	Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
	List<String> savedMedicineNames1 = ptp.getAllMedicinesNamesInMedicineTableAsList(Login_Doctor.driver, logger);
	Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
	Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
	Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger),"select template",Login_Doctor.driver,logger);
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
	List<String> savedMedicineNames2 = ptp.getAllMedicinesNamesInMedicineTableAsList(Login_Doctor.driver, logger);
	assertTrue(savedMedicineNames1.equals(savedMedicineNames2));
	}
		
	@Test(groups= {"Regression","PracticeManagement"},priority=444)
	public synchronized void unselectAllMedicineMastersAndValidateError()throws Exception{
		logger = Reports.extent.createTest("EMR unselect all medicine masters and validate");
		PrescriptionPage prescription = new PrescriptionPage();
		selectCIMSMedicine();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, prescription.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(prescription.getMessage(Login_Doctor.driver, logger), "Get Master removal error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("At least one drug database should be selected"));
	}
	
	//PM_12,PM_13  covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=445)
	public synchronized void savedTemplatePresentInList()throws Exception{
		logger = Reports.extent.createTest("EMR validate display of saved template ");
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		PrescriptionTest.templateName = templateName;
		Web_GeneralFunctions.clear(ptp.getTemplateSearchField(Login_Doctor.driver, logger), "clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ptp.getTemplateSearchField(Login_Doctor.driver,logger), "click on template search field", Login_Doctor.driver, logger);
		List<WebElement> templatesList = ptp.getAllSavedTemplatesAsList(Login_Doctor.driver, logger);
		assertTrue(!templatesList.isEmpty());
		String text = Web_GeneralFunctions.getText(pPage.getTemplateName(templateName, Login_Doctor.driver, logger), "Get searched template name", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase(templateName));
		
	}
	
	//PM_14 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=446)
	public synchronized void verifyEmptySearchResultWithUnsavedTemplateName() throws InterruptedException{
		logger = Reports.extent.createTest("EMR verify Empty search Result display with invalid template name search");
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		String randomSearchText = RandomStringUtils.randomAlphabetic(8);
		Web_GeneralFunctions.clear(ptp.getTemplateSearchField(Login_Doctor.driver, logger), "clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(ptp.getTemplateSearchField(Login_Doctor.driver, logger), randomSearchText, "searching with unsaved template name", Login_Doctor.driver, logger);
		List<WebElement> templatesList = ptp.getAllSavedTemplatesAsList(Login_Doctor.driver, logger);
		assertTrue(templatesList ==null);
	
	}
	
		//PM_50 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=447)
	public synchronized void selectAndVerifySavedTempalteDisplayInSearch()throws Exception{
		logger = Reports.extent.createTest("EMR verify saved template display in search results");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		PrescriptionTest.templateName = templateName;
		Web_GeneralFunctions.clear(ptp.getTemplateSearchField(Login_Doctor.driver, logger), "clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "Clear the value present in search field", Login_Doctor.driver, logger);
		String extractedTemplateName = Web_GeneralFunctions.getText(pPage.getTemplateName(templateName, Login_Doctor.driver, logger), "get templateName text", Login_Doctor.driver, logger);
		assertTrue(extractedTemplateName.equalsIgnoreCase(templateName));
		Web_GeneralFunctions.clearWebElement(ptp.getTemplateSearchField(Login_Doctor.driver, logger),"clear template serach field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger), "Get searched template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getUpdateTemplateButton(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=448)
	public synchronized void updateMedicineTemplateWithMedicineAndEmptyFrequnecyAndDuration()throws Exception{
		
		logger = Reports.extent.createTest("EMR Update medicine template with medicine and empty frequnecy and duration ");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		pTest.addMedicine();
		String attribute = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of new row", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(attribute);
		String freeText = RandomStringUtils.randomAlphabetic(7);
		Web_GeneralFunctions.sendkeys(pPage.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), freeText, "sending free text", Login_Doctor.driver, logger);
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
		PrescriptionTest.updateTemplateMedicine = freeText.substring(0, 1).toUpperCase()+freeText.substring(1) + " ";
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=449)
	public synchronized void updateFrequnecy()throws Exception{
		logger = Reports.extent.createTest("EMR Update medicine template with medicine and frequnecy and empty duration ");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		pTest.enterFrequency();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
			
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=450)
	public synchronized void updateDuration()throws Exception{
		logger = Reports.extent.createTest("EMR Update medicine template with mandatory fields ");
		PrescriptionPage pPage = new PrescriptionPage();
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		pTest.enterDuration();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
			assertTrue(text.equalsIgnoreCase("Prescription Template Updated Successfully"));
		}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=451)
	public synchronized void updateDose()throws Exception{
		logger = Reports.extent.createTest("EMR Update dose with empty dose unit");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		pTest.enterDose();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory field(s)"));
			
	}
	
	//PM_48 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=452)
	public synchronized void updateTemplateWithDoseUnit()throws Exception{
		logger = Reports.extent.createTest("EMR Update dose with  dose unit");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		pTest.selectDoseUnit();
		assertTrue(Web_GeneralFunctions.isDisplayed(templatePages.getUpdateTemplateButton(Login_Doctor.driver, logger)));
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Prescription Template Updated Successfully"));
	}
	
	//PM_50,PM_48  covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=453)
	public synchronized void updatedValuesReflectInTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR Update values reflect in template");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionPage pPage = new PrescriptionPage();
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "clear the template name", Login_Doctor.driver, logger);
		selectAndVerifySavedTempalteDisplayInSearch();
		assertTrue(Web_GeneralFunctions.isDisplayed(templatePages.getUpdateTemplateButton(Login_Doctor.driver, logger)));
		String attribute = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of new row", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(attribute);
		String updatedValues = Web_GeneralFunctions.getAttribute(pPage.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		updatedValues += " "+Web_GeneralFunctions.getAttribute(pPage.getFrequency(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		updatedValues += " "+Web_GeneralFunctions.getAttribute(pPage.getDuration(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		updatedValues += " "+Web_GeneralFunctions.getAttribute(pPage.getDoseField(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		updatedValues += " "+Web_GeneralFunctions.getAttribute(pPage.getDoseUnitField(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		assertTrue(updatedValues.equalsIgnoreCase(PrescriptionTest.updateTemplateMedicine));
		
	}
	
	//PM_49,PM_50
	@Test(groups= {"Regression","PracticeManagement"},priority=454)
	public synchronized void deleteMedicineTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR delete medicine");
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		createTemplate();
		String name = templateName;
		templateName = RandomStringUtils.randomAlphabetic(5)+"Duplicate";
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateNameField(Login_Doctor.driver, logger), "clear template name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(templatePages.getTemplateNameField(Login_Doctor.driver, logger), templateName, "sending template name", Login_Doctor.driver, logger);
		String id = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of the attribute", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(id);
		Web_GeneralFunctions.click(templatePages.select1stMedicineFromDropDown(id, Login_Doctor.driver, logger), "click 1st element", Login_Doctor.driver, logger);
		pTest.enterFrequency();
		pTest.enterDuration();
		PrescriptionTest.row++;
		pTest.addMedicine();
		Web_GeneralFunctions.click(templatePages.select1stMedicineFromDropDown(PrescriptionTest.row.toString(), Login_Doctor.driver, logger), "click 1st element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,pPage.getNOButtonInDuplicateBox(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pPage.getNOButtonInDuplicateBox(Login_Doctor.driver, logger), "Click no button in duplicate pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,pPage.getDeleteRow(PrescriptionTest.row, Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(pPage.getDeleteRow(PrescriptionTest.row, Login_Doctor.driver, logger), "delete the row", Login_Doctor.driver, logger);
		saveTemplate();
		selectAndVerifySavedTempalteDisplayInSearch();
		pTest.addMedicine();
		id = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of the attribute", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(id);
		Web_GeneralFunctions.click(templatePages.select1stMedicineFromDropDown(id, Login_Doctor.driver, logger), "click 1st element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getYesButtonInDuplicateBox(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pPage.getYesButtonInDuplicateBox(Login_Doctor.driver, logger), "Click yes button in duplicate pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		pTest.enterFrequency();
		pTest.enterDuration();
		updateTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pPage.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.clear(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "clear template search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "Clear the template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger), "Get searched template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, templatePages.getDeleteTemplateButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(templatePages.getDeleteTemplateButton(Login_Doctor.driver, logger)));
		clickDeleteTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, templatePages.getYesInDeleteTemplatePopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(templatePages.getYesInDeleteTemplatePopUp(Login_Doctor.driver, logger), "click yes button in delete pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, templatePages.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(templatePages.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Prescription Template Deleted Successfully"));
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "Clear the template name", Login_Doctor.driver, logger);
		assertTrue(pPage.getTemplateName(templateName, Login_Doctor.driver, logger)==null);
		templateName = name;
		Web_GeneralFunctions.clearWebElement(templatePages.getTemplateSearchField(Login_Doctor.driver, logger), "Clear the template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pPage.getTemplateName(templateName, Login_Doctor.driver, logger), "Get searched template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, templatePages.getDeleteTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, templatePages.getUpdateTemplateButton(Login_Doctor.driver, logger));
		String pid = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of the attribute", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(pid);
		savedMedicineTableDetails = "";
		for(int i=0;i<PrescriptionTest.row+1;i++) {
			savedMedicineTableDetails+= Web_GeneralFunctions.getAttribute(pPage.getDrugName(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			savedMedicineTableDetails+=Web_GeneralFunctions.getAttribute(pPage.getFrequency(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			savedMedicineTableDetails+=Web_GeneralFunctions.getAttribute(pPage.getDuration(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			savedMedicineTableDetails+=Web_GeneralFunctions.getAttribute(pPage.getDoseField(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			savedMedicineTableDetails+=Web_GeneralFunctions.getAttribute(pPage.getDoseUnitField(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			
	}
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=455)
	public synchronized void applyCreatedTemplateInConsultationPage()throws Exception{
		logger = Reports.extent.createTest("EMR apply created template in consultation page");
		//from consultation package
		PrescriptionTemplatePages templatePages = new PrescriptionTemplatePages();
		PrescriptionTest pTest = new PrescriptionTest();
		PrescriptionPage pPage = new PrescriptionPage();
		VitalMasterTest vmt = new VitalMasterTest();
		GlobalPrintPage gpp = new GlobalPrintPage();
		PracticeManagementTest pmt = new PracticeManagementTest();
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		vmt.startConsultation();
		pTest.moveToPrescriptionModule();
		PrescriptionTest.templateName = templateName;
		Web_GeneralFunctions.click(pPage.getTemplateFromDropDown(templateName, Login_Doctor.driver, logger), "Get template from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		pTest.moveToPrescriptionModule();
		String id = Web_GeneralFunctions.getAttribute(templatePages.getId(Login_Doctor.driver, logger), "id", "get id of the attribute", Login_Doctor.driver, logger);
		PrescriptionTest.row = Integer.parseInt(id);
		String medicineTableDetailsInConsultatoinPage = "";
		Web_GeneralFunctions.wait(1);
		for(int i=0;i<PrescriptionTest.row+1;i++) {
			medicineTableDetailsInConsultatoinPage+= Web_GeneralFunctions.getAttribute(pPage.getDrugName(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			medicineTableDetailsInConsultatoinPage+=Web_GeneralFunctions.getAttribute(pPage.getFrequency(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			medicineTableDetailsInConsultatoinPage+=Web_GeneralFunctions.getAttribute(pPage.getDuration(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			medicineTableDetailsInConsultatoinPage+=Web_GeneralFunctions.getAttribute(pPage.getDoseField(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
			medicineTableDetailsInConsultatoinPage+=Web_GeneralFunctions.getAttribute(pPage.getDoseUnitField(i, Login_Doctor.driver, logger), "value", "get the drug name", Login_Doctor.driver, logger);
		}
		backToPrescriptionTemplateModule();
		selectClinicSpecificMedicine();
		selectTDHMedicine();
		assertTrue(medicineTableDetailsInConsultatoinPage.equals(savedMedicineTableDetails));
	}
	
	public synchronized void backToPrescriptionTemplateModule()throws Exception{
		logger = Reports.extent.createTest("EMR Back to prescription template from consultation page");
		PracticeManagementTest pmt = new PracticeManagementTest();
		pmt.moveToPracticeManagement();
		moveToPrescriptionTemplateMenu();
		Web_GeneralFunctions.wait(1);
	}
	
	 public synchronized void chooseOnlyTDHMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR choose tdh and search tdh medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		selectCIMSMedicine();
		selectClinicSpecificMedicine();
		Web_GeneralFunctions.click(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "Get drug name row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> allDrugSuggestionCategories = ptp.getAllMedicineCategorySuggestionNamesAsList(Login_Doctor.driver, logger);
		for(String category :allDrugSuggestionCategories) {
		assertTrue(!(category.contains("CIMS")) && !(category.contains("Clinic Specific")));
		}
		String text = Web_GeneralFunctions.getText(prescription.getMedicinMasterFromDropDown(PrescriptionTest.row, "", Login_Doctor.driver, logger), "Get 1st medicine master", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("TDH Catalog"));
		assertTrue(Web_GeneralFunctions.isDisplayed(ptp.getMedicineComposition(PrescriptionTest.row, Login_Doctor.driver, logger)));
	}
	
	 public synchronized void chooseOnlyCinicSpecificMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR choose tdh and search tdh medicine");
		PrescriptionPage prescription = new PrescriptionPage();
		PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
		selectCIMSMedicine();
		selectTDHMedicine();
		Web_GeneralFunctions.click(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "Get drug name row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineElementsAsList(Login_Doctor.driver, logger).get(1));
		List<String> allDrugSuggestionCategories = ptp.getAllMedicineCategorySuggestionNamesAsList(Login_Doctor.driver, logger);
		for(String category :allDrugSuggestionCategories) {
			assertTrue(!(category.contains("CIMS")) && !(category.contains("TDH Catalog")));
		}
		String text = Web_GeneralFunctions.getText(prescription.getMedicinMasterFromDropDown(PrescriptionTest.row, "", Login_Doctor.driver, logger), "Get 1st medicine master", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Clinic Specific"));
 		}
	
	public synchronized void chooseOnlyCIMSMedicine()throws Exception{ 
		  PrescriptionTest.row++; 
		  logger = Reports.extent.createTest("EMR choose CIMS and search CIMS medicine");
	  PrescriptionPage prescription = new PrescriptionPage();
	  PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	   selectCIMSMedicine();
	  selectTDHMedicine();
	   new PrescriptionTest().addMedicine();
	   Web_GeneralFunctions.click(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver,logger), "Get drug name row", Login_Doctor.driver, logger);
	   Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAllMedicineElementsAsList(Login_Doctor.driver, logger).get(1));
		 List<String> allDrugSuggestionCategories = ptp.getAllMedicineCategorySuggestionNamesAsList(Login_Doctor.driver, logger);
		for(String category :allDrugSuggestionCategories) {
			assertTrue(!(category.contains("TDH Catalog")) && !(category.contains("Clinic Specific")));
		}
	 Web_GeneralFunctions.click(prescription.getFirstMedicineFromDropDown(PrescriptionTest.row, PrescriptionTest.cimsMedicineName,Login_Doctor.driver, logger), "Get 1st medicine", Login_Doctor.driver,logger);
	  }
	
	public synchronized void selectCIMSMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR Check/Uncheck CIMS check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		WebElement cimscheckbox = prescription.getCIMSCheckBox(Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementByJavaScriptExecutor(cimscheckbox, "scrolling to view cims checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(cimscheckbox,"Check/Uncheck CIMS medicine", Login_Doctor.driver, logger);
	}
	
	public synchronized void selectTDHMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR Check/Uncheck TDH check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.click(prescription.getTDHCheckBox(Login_Doctor.driver, logger),"Check/Uncheck TDH medicine", Login_Doctor.driver, logger);
	}
	
	public synchronized void selectClinicSpecificMedicine()throws Exception{
		logger = Reports.extent.createTest("EMR Check/Uncheck clinic specific check box ");
		PrescriptionPage prescription = new PrescriptionPage();
		Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger),"Check/Uncheck clinic specific medicine", Login_Doctor.driver, logger);
	}
	
	public synchronized void createTemplate()throws Exception{
	logger = Reports.extent.createTest("EMR click on create template button");
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.click(ptp.getCreateTemplate(Login_Doctor.driver, logger), "Click Create Template button", Login_Doctor.driver, logger);
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getSaveTemplateButton(Login_Doctor.driver, logger));
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getAddMedicineButton(Login_Doctor.driver, logger));
	
	}

	public synchronized void saveTemplate()throws Exception{
	logger = Reports.extent.createTest("EMR click save template button");
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.click(ptp.getSaveTemplateButton(Login_Doctor.driver, logger), "Click save Template button", Login_Doctor.driver, logger);
	
	}

	public synchronized void updateTemplate()throws Exception{
	logger = Reports.extent.createTest("EMR click update template button");
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.click(ptp.getUpdateTemplateButton(Login_Doctor.driver, logger), "Click update Template button", Login_Doctor.driver, logger);
	
	}

	public synchronized void clickDeleteTemplate()throws Exception{
	
	logger = Reports.extent.createTest("EMR click delete template button");
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.click(ptp.getDeleteTemplateButton(Login_Doctor.driver, logger), "Click update Template button", Login_Doctor.driver, logger);
	
}

	public synchronized void moveToPrescriptionTemplateMenu()throws Exception{
	logger = Reports.extent.createTest("EMR move to prescription menu");
	PrescriptionTemplatePages ptp = new PrescriptionTemplatePages();
	Web_GeneralFunctions.click(ptp.getPrescriptionMenu(Login_Doctor.driver, logger), "Click Prescription template menu button", Login_Doctor.driver, logger);
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getCreateTemplate(Login_Doctor.driver, logger));
	Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ptp.getTemplateSearchField(Login_Doctor.driver, logger));
	}


	
	
}

