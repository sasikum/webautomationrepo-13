package com.tatahealth.EMR.Scripts.ReportsView;

import java.util.Random;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload6 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;

	Random random = new Random();

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}
	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}
	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 51, groups = { "Regression", "ReportView" })
	public void clickOnReportsViewFromSearch() throws Exception {
		logger = Reports.extent.createTest("To check when user click on view report from search result ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(1);
		String expectedPage = Login_Doctor.driver.getCurrentUrl();
		String mobileNumber = RandomStringUtils.randomNumeric(10);
		String UHID = RandomStringUtils.randomNumeric(5);
		String name = RandomStringUtils.randomAlphabetic(6);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getNameField(Login_Doctor.driver, logger).sendKeys(name);
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(mobileNumber);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		reportsViewUploadLunch();
		String actualPage = Login_Doctor.driver.getCurrentUrl();
		try {
			Assert.assertEquals(actualPage, expectedPage);
		} catch (Exception e) {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 52, groups = { "Regression", "ReportView" })
	public void checkSearchedPatientsDetails() throws Exception {
		logger = Reports.extent.createTest("To check what all the information should display in view report page");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String UHID = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		report.getViewReports(Login_Doctor.driver, logger).click();
		Assert.assertTrue(report.getPatientAge(Login_Doctor.driver, logger).isDisplayed(), "UHID Field displayed");
		Assert.assertTrue(report.getAllergies(Login_Doctor.driver, logger).isDisplayed(), "UHID Field displayed");
		Assert.assertTrue(report.getListOfRequest(Login_Doctor.driver, logger).isDisplayed(), "UHID Field displayed");
	}
	@Test(priority = 53, groups = { "Regression", "ReportView" })
	public void columnsInListOfRequest() throws Exception {
		logger = Reports.extent.createTest("To check what all the column should display in list of requests");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		String UHID = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		report.getViewReports(Login_Doctor.driver, logger).click();
		Assert.assertTrue(report.getSerialNumber(Login_Doctor.driver, logger).isDisplayed(), "S.No column displayed");
		Assert.assertTrue(report.getTestName(Login_Doctor.driver, logger).isDisplayed(), "Test Name column displayed");
		Assert.assertTrue(report.getRecommendedBy(Login_Doctor.driver, logger).isDisplayed(), "Recommendate date column displayed");
		Assert.assertTrue(report.getRecommendationDate(Login_Doctor.driver, logger).isDisplayed(), "Recommendated By column displayed");
		Assert.assertTrue(report.getActions(Login_Doctor.driver, logger).isDisplayed(), "Actions column displayed");
	}
	@Test(priority = 54, groups = { "Regression", "ReportView" })
	public void verifyPatientDetailsPage() throws Exception {
		logger = Reports.extent.createTest("To check when user click on user image/profile in detailed report page  ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		String UHID = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getPatientIcon(Login_Doctor.driver, logger).click();
		String actualModalTitle = report.getPatientDetailsModal(Login_Doctor.driver, logger).getText();
		try {
			Assert.assertEquals(actualModalTitle, "Patient Details");
		} catch (Exception e) {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 55, groups = { "Regression", "ReportView" })
	public void closePatientDetialsModal() throws Exception {
		logger = Reports.extent.createTest("To check whether user can be able to close patient detail pop up from detailed view  report page");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		String UHID = report.getMobileNumber(Login_Doctor.driver, logger).getText();
		report.getMobileField(Login_Doctor.driver, logger).sendKeys(UHID);
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		report.getViewReports(Login_Doctor.driver, logger).click();
		report.getPatientIcon(Login_Doctor.driver, logger).click();
		try {
			Assert.assertTrue(report.getPatientIcon(Login_Doctor.driver, logger).isDisplayed(), "User image/profile in detailed report page ");
		} catch (Exception e) {
			Assert.assertTrue(false);
		}	
	}
	@Test(priority = 56, groups = { "Regression", "ReportView" })
	public void miscellaneousDisplay() throws Exception {
		logger = Reports.extent.createTest("Verify the list of requests while doing consultation when doctor enters lab results");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(2);
		report.getUHIDField(Login_Doctor.driver, logger).sendKeys("8117");
		report.getSeachPatientBtn(Login_Doctor.driver, logger).click();
		report.getViewReports(Login_Doctor.driver, logger).click();
		Assert.assertEquals(report.getPatientsTestName(Login_Doctor.driver, logger).getText(), "Miscellaneous");
	}
}
